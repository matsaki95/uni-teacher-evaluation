<?php

function connect_to_db()
{
    if (!function_exists('mysqli_init') && !extension_loaded('mysqli')) {
        die('Dependency error: mysqli is not installed and/or loaded');
    }

    $db_servername = "localhost";
    $db_name = "teacher_evaluation";
    $db_username = "evaluation";
    $db_password = "stronk_password!";

    $db_conn = new mysqli($db_servername, $db_username, $db_password, $db_name);

    if ($db_conn->connect_error) {
        die("Connection failed: " . $db_conn->connect_error);
    }

    return $db_conn;
}

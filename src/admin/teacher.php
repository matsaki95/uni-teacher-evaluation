<?php
session_start();

if (!isset($_SESSION['user_type']) || $_SESSION['user_type'] != 'admin') {
    header('Location: /index.php');
    exit();
}

if (!isset($_GET['id'])) {
    header("Location: /index.php");
    exit();
}
$teacher_id = intval($_GET['id']);

require_once('../connect-db.php');
$db_conn = connect_to_db();
$query = "SELECT User.name, User.surname
          FROM User
          WHERE User.id = $teacher_id AND User.type='teacher'";
$result = $db_conn->query($query);
$db_conn->close();
$row = $result->fetch_row();
if (!$row) {
    header("Location: /index.php"); // Maybe something like a 404 or equivalent page would be better
    exit();
}
$teacher_name = $row[0];
$teacher_surname = $row[1];
?>

<!doctype html>

<html lang="gr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title> Σύστημα Αξιολόγησης Καθηγητών - Θυρίδα Διαχειριστή </title>
    <meta name="description" content="A simple HTML5 Template for new projects.">
    <meta name="author" content="Maciej Ratkiewicz">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
</head>

<body class="container-lg">
<div class="text-center mb-5">
    <h1 class="mb-4"> Σύστημα Αξιολόγησης Καθηγητών </h1>
    <h2> Επισκόπηση καθηγητή <?= $teacher_name . ' ' . $teacher_surname ?> </h2>
</div>

<table class="table table-hover mx-auto w-auto">
    <thead>
    <tr>
        <th> Έτος</th>
        <th> Μέσος όρος απαντήσεων</th>
    </tr>
    </thead>
    <tbody>
    <?php require_once('../stat-functions/teacher.php');
    $years = get_teacher_yearly_averages_array($teacher_id);
    foreach ($years as $year => $average_rating) {
        echo "<tr><td><a href='teacher-year.php?id=$teacher_id&year=$year'> $year </a></td> <td> $average_rating </td></tr>";
    }
    ?>
    </tbody>
</table>

</body>
</html>

<?php
session_start();

if (!isset($_SESSION['user_type']) || $_SESSION['user_type'] != 'admin') {
    header('Location: /index.php');
    exit();
}

$current_year = date('Y');
?>

<!doctype html>

<html lang="gr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title> Σύστημα Αξιολόγησης Καθηγητών - Θυρίδα Διαχειριστή </title>
    <meta name="description" content="A simple HTML5 Template for new projects.">
    <meta name="author" content="Maciej Ratkiewicz">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
</head>

<body class="container-lg">
<div class="text-center mb-5">
    <h1 class="mb-4"> Σύστημα Αξιολόγησης Καθηγητών </h1>
    <h2> Θυρίδα διαχειριστή <?= $_SESSION["user_name"] . ' ' . $_SESSION["user_surname"] ?></h2>
</div>

<h4 class="text-center"> Καθηγητές</h4>
<table class="table table-hover mx-auto w-auto">
    <thead>
    <tr>
        <th> Μάθημα</th>
        <th> Φετινός μέσος όρος</th>
        <th> Περσινός μέσος όρος</th>
    </tr>
    </thead>
    <tbody>
    <?php require_once('../stat-functions/teacher.php');
    $teachers_current = get_all_teacher_averages_for_year_array($current_year);
    $teachers_last_year = get_all_teacher_averages_for_year_array($current_year - 1);
    foreach ($teachers_current as $teacher_id => $teacher_current) {
        $teacher_name = $teacher_current['name'];
        $teacher_surname = $teacher_current['surname'];
        $current_average_rating = $teacher_current['average_rating'];
        $last_average_rating = $teachers_last_year[$teacher_id]['average_rating'];
        echo "<tr><td><a href='teacher.php?id=$teacher_id'> $teacher_name $teacher_surname </a></td>
                  <td> $current_average_rating </td>
                  <td> $last_average_rating </td>
              </tr>";
    }
    ?>
    </tbody>
</table>

</body>
</html>

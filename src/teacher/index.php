<?php
session_start();

if (!isset($_SESSION['user_type']) || $_SESSION['user_type'] != 'teacher') {
    header('Location: /index.php');
    exit();
}

$current_year = date('Y');
?>

<!doctype html>

<html lang="gr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title> Σύστημα Αξιολόγησης Καθηγητών - Θυρίδα Καθηγητή </title>
    <meta name="description" content="A simple HTML5 Template for new projects.">
    <meta name="author" content="Maciej Ratkiewicz">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
</head>

<body class="container-lg">
<div class="text-center mb-5">
    <h1> Σύστημα Αξιολόγησης Καθηγητών </h1>
    <h2> Θυρίδα καθηγητή <?= $_SESSION["user_name"] . ' ' . $_SESSION["user_surname"] ?></h2>
</div>

<div class="row justify-content-evenly">
    <div class="col-sm-6 mb-4">
        <h4> Επιλέξτε ενα μάθημα για να δημιουργήσετε κωδικούς αξιολόγησης </h4>
        <form action="create-codes.php" method="post">
            <div class="row">
                <div class="col-md-5 col-form-label">
                    <label for="course_id" class="form-label"> Μάθημα </label>
                </div>
                <div class="col-md-6">
                    <select id="course_id" name="course_id" class="form-select">
                        <?php require_once('../stat-functions/teacher.php');
                        $courses = get_courses_of_teacher($_SESSION['user_id']);
                        foreach ($courses as $course_id => $course_name) {
                            echo "<option value='$course_id'> $course_name </option>";
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col-md-5 col-form-label">
                    <label for="amount" class="form-label"> Ποσότητα κωδικών </label>
                </div>
                <div class="col-md-6">
                    <input type="number" id="amount" name="amount" value="50" class="form-control"/>
                </div>
            </div>
            <div class="text-center">
                <input type="submit" value="Δημιουργία" class="btn btn-primary mt-3"/>
            </div>
        </form>
    </div>

    <div class="col-sm-4 mb-4">
        <h4> Φετινός μέσος όρος </h4>
            <?php require_once('../stat-functions/teacher.php');
            $average_rating = get_teacher_average_for_year($_SESSION['user_id'], $current_year);
            if ($average_rating == null) {
                echo "<p> ανύπαρκτος προς το παρόν </p>";
            } else {
                echo "<p class='fs-2'> $average_rating </p>";
            }
            ?>
    </div>
</div>

<div class="row justify-content-evenly">
    <div class="col-sm-6 mb-4">
        <h4> Φετινές αξιολογήσεις </h4>
        <table class="table table-hover">
            <thead>
            <tr>
                <th> Μάθημα</th>
                <th> Μέσος όρος απαντήσεων</th>
            </tr>
            </thead>
            <tbody>
            <?php require_once('../stat-functions/course.php');
            $courses = get_all_courses_averages_for_teacher_and_year_array($_SESSION['user_id'], $current_year);
            foreach ($courses as $course_id => $course) {
                echo "<tr><td><a href='course.php?id=$course_id'>" . $course['name'] . "</a></td><td>" . $course['average_rating'] . "</td></tr>";
            }
            ?>
            </tbody>
        </table>
    </div>

    <div class="col-sm-4 mb-4">
        <h4> Παλιότερες χρονιές </h4>
        <table class="table table-hover">
            <thead>
            <tr>
                <th> Χρονιά</th>
                <th> Μέσος όρος απαντήσεων</th>
            </tr>
            </thead>
            <?php require_once('../stat-functions/course.php');
            $years = get_teacher_yearly_averages_array($_SESSION['user_id']);
            foreach ($years as $year => $average_rating) {
                if ($year == $current_year) {
                    continue;
                }
                echo "<tr><td><a href='year.php?value=$year'> $year </a></td> <td> $average_rating </td></tr>";
            }
            ?>
        </table>
    </div>
</div>

</body>
</html>

<?php
session_start();

if (!isset($_SESSION['user_type']) || $_SESSION['user_type'] != 'teacher') {
    header('Location: /index.php');
    exit();
}

if (!isset($_GET['value'])) {
    header("Location: /index.php");
    exit();
}
$year = intval($_GET['value']);
?>

<!doctype html>

<html lang="gr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title> Σύστημα Αξιολόγησης Καθηγητών - Θυρίδα Καθηγητή </title>
    <meta name="description" content="A simple HTML5 Template for new projects.">
    <meta name="author" content="Maciej Ratkiewicz">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
</head>

<body class="container-lg">
<div class="text-center mb-5">
    <h1> Σύστημα Αξιολόγησης Καθηγητών </h1>
    <h2> Επισκόπηση έτους <?= $year ?> </h2>
    <h3> Για τον/ην <?= $_SESSION['user_name'] . ' ' . $_SESSION['user_surname'] ?> </h3>
</div>

<table class="table table-hover mx-auto w-auto">
    <thead>
    <tr>
        <th> Μάθημα</th>
        <th> Μέσος όρος απαντήσεων</th>
    </tr>
    </thead>
    <tbody>
    <?php require_once('../stat-functions/course.php');
    $courses = get_all_courses_averages_for_teacher_and_year_array($_SESSION['user_id'], $year);
    foreach ($courses as $course_id => $course) {
        echo "<tr><td><a href='course.php?id=$course_id&year=$year'>" . $course['name'] . "</a></td><td>" . $course['average_rating'] . "</td></tr>";
    }
    ?>
    </tbody>
</table>

</body>
</html>

<?php
session_start();

if (!isset($_SESSION['user_type']) || $_SESSION['user_type'] != 'teacher') {
    header('Location: /index.php');
    exit();
}

if (!isset($_POST['course_id']) || !isset($_POST['amount'])) {
    header('Location: index.php');
    exit();
}
$course_id = $_POST['course_id'];
$amount = $_POST['amount'];
$current_year = date('Y');

require_once('../connect-db.php');
$db_conn = connect_to_db();

// Check if the course is being taught by the logged in teacher
$query = "SELECT Course.name
          FROM Course
          WHERE Course.id = $course_id AND Course.teacher = " . $_SESSION['user_id'];
$result = $db_conn->query($query);
if ($result->num_rows != 1) {
    header('Location: index.php');
    exit();
}
$course_name = $result->fetch_row()[0];

// We need to make sure the codes we generate don't clash with existing codes
$code_start = "";
do {
    $code_start = $current_year . '-' . rand(1000, 9999) . '-';
    $query = "SELECT Code.id
              FROM Code
              WHERE Code.code LIKE '$code_start%'";
    $result = $db_conn->query($query);
} while ($result->num_rows != 0);

$codes = array();
for ($i = 0; $i < $amount; $i += 1) {
    $codes [] = $code_start . $i;
}

$query = "INSERT INTO Code (course, code, year_of_validity) VALUES ";
foreach ($codes as $code) {
    $query .= "($course_id, '$code', $current_year),";
}
$query = substr($query, 0, -1); // Basically pop the last comma
$result = $db_conn->query($query);
$db_conn->close();
?>

<!doctype html>

<html lang="gr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title> Σύστημα Αξιολόγησης Καθηγητών - Θυρίδα Καθηγητή </title>
    <meta name="description" content="A simple HTML5 Template for new projects.">
    <meta name="author" content="Maciej Ratkiewicz">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
</head>

<body class="container-lg text-center">
<div class="mb-5">
    <h1> Σύστημα Αξιολόγησης Καθηγητών </h1>
    <h2> Δημιουργία <?= $amount ?> κωδικών για το μάθημα <?= $course_name ?> </h2>
</div>

<?php
if (!$result) {
    echo "<h4 class='text-danger'> Δεν μπόρεσαν να εισαχθούν οι κωδικοί στο σύστημα </h4>";
} else {
    echo "<h4> Οι κωδικοί που δημιουργηθήκαν: </h4><table class='table table-hover mx-auto w-auto'>";
    foreach ($codes as $code) {
        echo "<tr><td class='user-select-all'> $code </td></tr>";
    }
    echo "</table>";
}
?>

</body>
</html>

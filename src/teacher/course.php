<?php
session_start();

if (!isset($_SESSION['user_type']) || $_SESSION['user_type'] != 'teacher') {
    header('Location: /index.php');
    exit();
}

if (!isset($_GET['id'])) {
    header("Location: /index.php");
    exit();
}
$course_id = intval($_GET['id']);

$year = 0;
if (isset($_GET['year'])) {
    $year = intval($_GET['year']);
} else {
    $year = date('Y');
}

// Validate that the course exists and is taught by the authenticated user
require_once('../connect-db.php');
$db_conn = connect_to_db();
$query = "SELECT Course.name
          FROM Course
          JOIN User ON Course.teacher = User.id
          WHERE Course.id = $course_id AND User.id = " . $_SESSION['user_id'];
$result = $db_conn->query($query);
$db_conn->close();
$row = $result->fetch_row();
if (!$row) {
    header("Location: /index.php"); // Maybe something like a 404 or equivalent page would be better
    exit();
}
$course_name = $row[0];
?>

<!doctype html>

<html lang="gr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title> Σύστημα Αξιολόγησης Καθηγητών - Θυρίδα Καθηγητή </title>
    <meta name="description" content="A simple HTML5 Template for new projects.">
    <meta name="author" content="Maciej Ratkiewicz">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">

</head>

<body class="container-lg">
<div class="text-center mb-5">
    <h1> Σύστημα Αξιολόγησης Καθηγητών </h1>
    <h2> Επισκόπηση μαθήματος <?= $course_name ?> </h2>
    <h3> Καθηγητής/τρια <?= $_SESSION['user_name'] . ' ' . $_SESSION['user_surname'] ?> </h3>
    <h3> Έτος <?= $year ?> </h3>
</div>

<table class="table table-hover mx-auto w-auto">
    <thead>
    <tr>
        <th> Ερώτηση</th>
        <th> Μέσος όρος απαντήσεων</th>
    </tr>
    </thead>
    <tbody>
    <?php require_once('../stat-functions/course.php');
    $questions = get_averages_for_questions_on_year($course_id, $year);
    foreach ($questions as $question_id => $question) {
        $prompt = $question['prompt'];
        $average_rating = $question['average_rating'];
        echo "<tr><td> $prompt </td> <td> $average_rating </td></tr>";
    }
    ?>
    </tbody>
</table>

</body>
</html>

<!doctype html>

<html lang="gr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title> Σύνδεση στο σύστημα αξιολόγησης </title>
    <meta name="description" content="A simple HTML5 Template for new projects.">
    <meta name="author" content="Maciej Ratkiewicz">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
</head>

<body class="text-center container-lg" style="height: 100vh">
<div class="row h-100 align-items-center">
    <div class="col">
        <h1 class="h1 mb-5"> Καλώς ορίσατε στο σύστημα αξιολόγησης </h1>
    </div>

    <div class="col">
        <form action="login-proccess.php" method="post">
            <h3 class="h3 mb-3"> Παρακαλώ συνδεθείτε με τον λογαριασμό και τον κωδικό σας </h3>
            <div class="form-floating">
                <input type="text" id="username" name="username" class="form-control" placeholder="someone@someplace.com"/>
                <label for="username"> Username </label>
            </div>
            <div class="form-floating">
                <input type="password" id="password" name="password" class="form-control" placeholder="password"/>
                <label for="password" class="form-label"> Password </label>
            </div>
            <?php
            if (key_exists("failed", $_GET)) {
                echo "<p class='text-danger fst-italic mb-0' > Τα στοιχεία που δώθηκαν δεν ήταν σωστά </p>";
            }
            ?>
            <input type="submit" value="Login" class="btn btn-lg btn-primary mt-3"/>
        </form>
    </div>
</div>
</body>
</html>


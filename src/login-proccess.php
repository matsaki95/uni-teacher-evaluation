<?php
require_once("connect-db.php");

session_start();

if (!isset($_POST["username"]) || !isset($_POST["password"])) {
    header("Location: /login.php");
    exit();
}
$username = $_POST["username"];
$password = $_POST["password"];

$db_conn = connect_to_db();
$query = "SELECT id, type, username, name, surname
          FROM User
          WHERE username='$username'
          AND password='$password'";
$result = $db_conn->query($query);

if ($result->num_rows == 0) {
    header("Location: login.php?failed");
    exit();
}
$user = $result->fetch_row();
$_SESSION["user_id"] = $user[0];
$_SESSION["user_type"] = $user[1];
$_SESSION["user_username"] = $user[2];
$_SESSION["user_name"] = $user[3];
$_SESSION["user_surname"] = $user[4];

header("Location: index.php");

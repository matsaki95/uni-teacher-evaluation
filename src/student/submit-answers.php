<?php

session_start();

if (!isset($_SESSION['user_type']) || $_SESSION['user_type'] != 'student') {
    header("Location: /index.php");
    exit();
}

// Unfortunately, since we can't trust user input,
// we have to repeat all the checks from before (in `evaluate.php`).
if (!isset($_POST['code'])) {
    header("Location: index.php?error_message=Πήγαν να εισαχθούν στο σύστημα απαντήσεις χωρίς κωδικό");
    exit();
}
$code = trim($_POST['code']);

require_once('../connect-db.php');
$db_conn = connect_to_db();

$query = "SELECT Code.id, Course.id, Code.used_by_student, Code.year_of_validity,
                 Course.name
          FROM Code
          JOIN Course ON Code.course = Course.id
          WHERE code = '$code'";
$result = $db_conn->query($query);

if ($result->num_rows == 0) {
    header("Location: index.php?error_message=Πήγαν να εισαχθούν στο σύστημα απαντήσεις για κωδικό που δεν υπάρχει");
    exit();
}
$row = $result->fetch_row();

$code_id = $row[0];
$course_id = $row[1];
$code_used_by_student = $row[2];
$code_year_of_validity = $row[3];
$course_name = $row[4];

if ($_SESSION["user_id"] == $code_used_by_student) {
    header("Location: index.php?error_message=Πήγαν να εισαχθούν στο σύστημα απαντήσεις με κωδικό που ήδη χρησιμοποιήσατε");
    exit();
}
if ($code_year_of_validity != date('Y')) {
    header("Location: index.php?error_message=Πήγαν να εισαχθούν στο σύστημα απαντήσεις με κωδικό που έχει ισχύ μόνο για το έτος $code_year_of_validity");
    exit();
}

$query = "SELECT id
          FROM Code
          WHERE course='$course_id'
          AND used_by_student='" . $_SESSION['user_id'] . "'
          AND year_of_validity='$code_year_of_validity'";
$result = $db_conn->query($query);
if ($result->num_rows != 0) {
    header("Location: index.php?error_message=Πήγαν να εισαχθούν στο σύστημα απαντήσεις για το μάθημα $course_name, για το οποίο έχετε ήδη κάνει αξιολόγηση");
    exit();
}
if ($code_used_by_student != null) {
    // This is at the end despite it being to be able to be done earlier and save us a db query
    // in order to lessen the amount of information one can (easily) extract about the usage of other codes.
    // One still can, but it's a bit harder this way.
    header("Location: index.php?error_message=Πήγαν να εισαχθούν στο σύστημα απαντήσεις με κωδικό που έχει ήδη χρησιμοποιηθεί από άλλον. Ίσως να σας πρόλαβαν στην χρήση του.");
    exit();
}

$question_ids = array();
$query = "SELECT id
          FROM Question";
$result = $db_conn->query($query);
if ($result->num_rows == 0) {
    header("Location: index.php?error_message=Πήγαν να εισαχθούν στο σύστημα απαντήσεις ενώ δεν υπάρχουν ερωτήσεις, ίσως το σύστημα να έχει πρόβλημα, παρακαλώ δοκιμάστε αργότερα");
    exit();
}
while ($row = $result->fetch_row()) {
    $question_ids[] = $row[0];
}

$query = "UPDATE Code SET used_by_student = " . $_SESSION["user_id"] . "
          WHERE id = $code_id";
$db_conn->query($query);

$query = "INSERT INTO Answer (question, course, rating, year) VALUES ";
foreach ($question_ids as $question_id) {
    if (!isset($_POST["choice_$question_id"])) {
        continue;
    }
    $answer = $_POST["choice_$question_id"];
    $answer = min(5, $answer);
    $answer = max(1, $answer);

    $query = $query . " ($question_id, $course_id, $answer, $code_year_of_validity),";
}
$query = substr($query, 0, -1); // Pop last char (the `,`)
$db_conn->query($query);

header("Location: index.php?success_message=Ευχαριστούμε για την συμμετοχή σας");

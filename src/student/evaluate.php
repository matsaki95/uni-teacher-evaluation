<?php

session_start();

if (!isset($_SESSION['user_type']) || $_SESSION['user_type'] != 'student') {
    header("Location: /index.php");
    exit();
}

if (!isset($_POST['code'])) {
    header("Location: /index.php");
    exit();
}
$code = trim($_POST['code']);

require_once('../connect-db.php');
$db_conn = connect_to_db();

$query = "SELECT Code.id, Course.id, Code.used_by_student, Code.year_of_validity,
                 Course.name,
                 Teacher.id, Teacher.name, Teacher.surname
          FROM Code
          JOIN Course ON Code.course = Course.id
          JOIN User as Teacher ON Course.teacher = Teacher.id
          WHERE code = '$code'";
$result = $db_conn->query($query);

if ($result->num_rows == 0) {
    header("Location: index.php?error_message=Ο κωδικός αυτός δεν υπάρχει");
    exit();
}
$row = $result->fetch_row();

$code_id = $row[0];
$course_id = $row[1];
$code_used_by_student = $row[2];
$code_year_of_validity = $row[3];
$course_name = $row[4];
$teacher_id = $row[5];
$teacher_name = $row[6];
$teacher_surname = $row[7];

if ($_SESSION["user_id"] == $code_used_by_student) {
    header("Location: index.php?error_message=Έχετε χρησιμοποιήσει ήδη τον κωδικό αυτό");
    exit();
}
if ($code_year_of_validity != date('Y')) {
    header("Location: index.php?error_message=Ο κωδικός έχει ισχύ μόνο για το έτος $code_year_of_validity");
    exit();
}

$query = "SELECT id
          FROM Code
          WHERE course='$course_id'
          AND used_by_student='" . $_SESSION['user_id'] . "'
          AND year_of_validity='$code_year_of_validity'";
$result = $db_conn->query($query);
if ($result->num_rows != 0) {
    header("Location: index.php?error_message=Έχετε ήδη κάνει αξιολόγηση για το μάθημα $course_name");
    exit();
}
if ($code_used_by_student != null) {
    // This is at the end despite it being to be able to be done earlier and save us a db query
    // in order to lessen the amount of information one can (easily) extract about the usage of other codes.
    // One still can, but it's a bit harder this way.
    header("Location: index.php?error_message=Ο κωδικός έχει χρησιμοποιηθεί ήδη");
    exit();
}

$questions = array();
$query = "SELECT id, prompt
          FROM Question
          ORDER BY order_position";
$result = $db_conn->query($query);
if ($result->num_rows == 0) {
    header("Location: index.php?error_message=Δυστυχώς δεν υπάρχουν ερωτήσεις για να απαντήσετε. Ίσως απλώς να μην υπάρχουν ακόμη. Παρακαλώ δοκιμάστε αργότερα.");
    exit();
}
while ($row = $result->fetch_row()) {
    $questions[$row[0]] = $row[1];
}
?>

<!doctype html>
<html lang="gr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title> Αξιολόγηση μαθήματος <?= "$course_name" ?> </title>
    <meta name="description" content="A simple HTML5 Template for new projects.">
    <meta name="author" content="Maciej Ratkiewicz">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
</head>

<body class="container-lg">
<div class="text-center">
    <h1> Αξιολόγηση μαθήματος <?= "$course_name" ?> </h1>
    <h2> Καθηγητής <?= $teacher_name . " " . $teacher_surname ?></h2>
    <h3 class="mb-5"> Έτος <?= $code_year_of_validity ?> </h3>
</div>

<p> Οι επιλογές από το 1 εως το 5 σημαίνουν
    "Διαφωνώ απόλυτα", "Διαφωνώ", "Ούτε συμφωνώ ούτε διαφωνώ", "Συμφωνώ", και "Συμφωνώ απόλυτα" αντίστοιχα</p>

<form action="submit-answers.php" method="post">
    <input type="hidden" id="code" name="code" value="<?= $code ?>"/>
    <?php
    foreach ($questions as $question_id => $question_prompt) {
        echo "<h5> $question_prompt </h5><div class='row justify-content-centerz text-centerz mb-3'>";
        for ($i = 1; $i <= 5; $i += 1) {
            echo "<div class='col-auto text-center'>
                      <input type='radio' id='radio_$question_id" . $i . "' name='choice_$question_id' value='$i' class='form-check-input'/>
                      <br/>
                      <label for='radio_$question_id" . $i . "' class='form-check-label'> $i </label>
                  </div>";
        }
        echo "</div>";
    }
    ?>
    <div class="text-center">
        <input type="submit" value="Καταχώρηση" class="btn btn-primary mb-5"/>
    </div>
</form>
</body>
</html>

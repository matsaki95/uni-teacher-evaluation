<?php
session_start();

if (!isset($_SESSION['user_type'])) {
    header('Location: /index.php');
    exit();
}
if ($_SESSION['user_type'] != 'student') {
    header('Location: /index.php');
    exit();
}
?>

<!doctype html>

<html lang="gr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title> Σύστημα Αξιολόγησης Καθηγητών - Θυρίδα Μαθητή </title>
    <meta name="description" content="A simple HTML5 Template for new projects.">
    <meta name="author" content="Maciej Ratkiewicz">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
</head>

<body class="text-center container-lg" style="height: 100vh;"
<div class="col">
    <h1> Σύστημα Αξιολόγησης Καθηγητών </h1>
    <h2 class="mb-5"> Θυρίδα μαθητή <?= $_SESSION["user_name"] . ' ' . $_SESSION["user_surname"] ?></h2>

    <h3 class="mb-3"> Εισάγετε έναν κωδικό για να ξεκινήσετε την αξιολόγηση </h3>
    <form action="evaluate.php" method="post">
        <div class="row justify-content-center">
            <div class="col-form-label col-auto px-0">
                <label for="code" class="form-label"> Κωδικός: </label>
            </div>
            <div class="col-9 col-sm-6 col-md-3">
                <input type="text" id="code" name="code" class="form-control"/>
            </div>
        </div>
        <div class="row justify-content-center">
        <?php
        if (isset($_GET["success_message"])) {
            echo "<p class='text-danger fst-italic mb-0 w-75'> " . $_GET['success_message'] . " </p>";
        }
        if (isset($_GET["error_message"])) {
            echo "<p class='text-danger fst-italic mb-0 w-75'> " . $_GET['error_message'] . " </p>";
        }
        ?>
        </div>
        <input type="submit" value="Αξιολόγηση" class="btn btn-primary btn-md mt-3"/>
    </form>

</div>
</body>
</html>

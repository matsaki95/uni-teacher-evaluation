<?php
require_once('../connect-db.php');

function get_course_average_for_year($course_id, $year)
{
    $course_id = intval($course_id);
    $year = intval($year);

    $db_conn = connect_to_db();
    $query = "SELECT AVG(Answer.rating)
              FROM Answer
              WHERE Answer.course = $course_id AND Answer.year = $year";
    $result = $db_conn->query($query);
    $db_conn->close();

    if ($row = $result->fetch_row()) {
        return $row[0];
    }
    return null;
}

function get_course_yearly_average_array($course_id)
{
    $course_id = intval($course_id);

    $db_conn = connect_to_db();
    $query = "SELECT Answer.year, AVG(Answer.rating) as average_rating
              FROM Answer
              WHERE Answer.course = $course_id
              GROUP BY Answer.year
              ORDER BY Answer.year DESC";
    $result = $db_conn->query($query);
    $db_conn->close();

    $years = array();
    while ($row = $result->fetch_row()) {
        $years[$row[0]] = $row[1];
    }
    return $years;
}

function get_all_courses_averages_for_teacher_and_year_array($teacher_id, $year)
{
    $teacher_id = intval($teacher_id);
    $year = intval($year);

    $db_conn = connect_to_db();
    $query = "SELECT Course.id, Course.name,
              AVG(Answer.rating) as average_rating
              FROM Course
              LEFT OUTER JOIN Answer ON Course.id = Answer.course AND Answer.year = $year
              WHERE Course.teacher = $teacher_id
              GROUP BY Course.id, Course.name, Answer.year
              ORDER BY Course.name";
    $result = $db_conn->query($query);
    $db_conn->close();

    $courses = array();
    while ($row = $result->fetch_row()) {
        $course_id = $row[0];
        $course_name = $row[1];
        $average_rating = $row[2];
        $courses[$course_id] = array(
            'name' => $course_name,
            'average_rating' => $average_rating
        );
    }
    return $courses;
}

function get_averages_for_questions_on_year($course_id, $year)
{
    $course_id = intval($course_id);
    $year = intval($year);

    $db_conn = connect_to_db();
    $query = "SELECT Question.id, Question.prompt,
              AVG(Answer.rating) as average_rating
              FROM Question
              LEFT OUTER JOIN Answer ON Question.id = Answer.question AND Answer.year = $year AND Answer.course = $course_id
              GROUP BY Question.id, Question.prompt, Question.order_position
              ORDER BY Question.order_position";
    $result = $db_conn->query($query);
    $db_conn->close();

    $questions = array();
    while ($row = $result->fetch_row()) {
        $question_id = $row[0];
        $question_prompt = $row[1];
        $average_rating = $row[2];
        $questions[$question_id] = array(
            'prompt' => $question_prompt,
            'average_rating' => $average_rating
        );
    }
    return $questions;
}

<?php
require_once('../connect-db.php');

function get_courses_of_teacher($teacher_id) {
    $teacher_id = intval($teacher_id);

    $db_conn = connect_to_db();
    $query = "SELECT Course.id, Course.name
              FROM Course
              WHERE Course.teacher = $teacher_id
              ORDER BY Course.name";
    $result = $db_conn->query($query);
    $db_conn->close();

    $courses = array();
    while ($row = $result->fetch_row()) {
        $courses[$row[0]] = $row[1];
    }
    return $courses;
}

function get_teacher_average_for_year($teacher_id, $year)
{
    $teacher_id = intval($teacher_id);
    $year = intval($year);

    $db_conn = connect_to_db();
    $query = "SELECT AVG(Answer.rating) as average_rating
            FROM Course
            JOIN Answer on Course.id = Answer.course
            WHERE Course.teacher = $teacher_id
            AND Answer.year = $year";
    $result = $db_conn->query($query);
    $db_conn->close();

    if ($row = $result->fetch_row()) {
        return $row[0];
    }
    return null;
}

function get_teacher_yearly_averages_array($teacher_id)
{
    $teacher_id = intval($teacher_id);

    $db_conn = connect_to_db();
    $query = "SELECT Answer.year, AVG(Answer.rating) as average_rating
              FROM Course
              JOIN Answer on Course.id = Answer.course
              WHERE Course.teacher = $teacher_id
              GROUP BY Answer.year
              ORDER BY Answer.year DESC";
    $result = $db_conn->query($query);
    $db_conn->close();

    $output = array();
    while ($row = $result->fetch_row()) {
        $year = $row[0];
        $average_rating = $row[1];
        $output[$year] = $average_rating;
    }
    return $output;
}

function get_all_teacher_averages_for_year_array($year)
{
    $year = intval($year);

    $db_conn = connect_to_db();
    $query = "SELECT User.id, User.name, User.surname,
              AVG(Answer.rating) as average_rating
              FROM User
              LEFT OUTER JOIN (Course INNER JOIN Answer ON Course.id = Answer.course AND Answer.year = $year) ON User.id=Course.teacher
              WHERE User.type = 'teacher' 
              GROUP BY User.id, User.name, User.surname
              ORDER BY average_rating DESC";
    $result = $db_conn->query($query);
    $db_conn->close();

    $output = array();
    while ($row = $result->fetch_row()) {
        $teacher_id = $row[0];
        $teacher_name = $row[1];
        $teacher_surname = $row[2];
        $average_rating = $row[3];
        $output[$teacher_id] = array(
            'name' => $teacher_name,
            'surname' => $teacher_surname,
            'average_rating' => $average_rating,
        );
    }
    return $output;
}

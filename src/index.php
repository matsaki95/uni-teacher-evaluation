<?php
require_once('connect-db.php');

session_start();

if (!isset($_SESSION['user_type'])) {
    header("Location: login.php");
    exit();
}
$user_type = $_SESSION['user_type'];

header("Location: $user_type/index.php");
exit();

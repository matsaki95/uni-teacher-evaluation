FROM php:8-apache

RUN docker-php-ext-install mysqli && docker-php-ext-enable mysqli

COPY src/ /var/www/html

RUN mv /var/www/html/connect-db.php ./have-to-move-this-for-it-to-work && \
    sed 's/\$db_servername *=.*/\$db_servername="database"\;/' ./have-to-move-this-for-it-to-work \
    > /var/www/html/connect-db.php && \
    rm ./have-to-move-this-for-it-to-work

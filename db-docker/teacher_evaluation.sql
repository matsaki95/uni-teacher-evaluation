-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: database:3306
-- Generation Time: Dec 17, 2022 at 03:58 PM
-- Server version: 10.10.2-MariaDB-1:10.10.2+maria~ubu2204
-- PHP Version: 8.0.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `teacher_evaluation`
--

-- --------------------------------------------------------

--
-- Table structure for table `Answer`
--

CREATE TABLE `Answer` (
  `question` int(11) NOT NULL,
  `course` int(11) NOT NULL,
  `rating` int(11) NOT NULL,
  `year` year(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `Answer`
--

INSERT INTO `Answer` (`question`, `course`, `rating`, `year`) VALUES
(6, 1, 4, 2022),
(1, 1, 4, 2022),
(2, 1, 4, 2022),
(3, 1, 3, 2022),
(7, 1, 4, 2022),
(8, 1, 5, 2022),
(9, 1, 1, 2022),
(10, 1, 1, 2022),
(6, 1, 2, 2022),
(1, 1, 4, 2022),
(2, 1, 3, 2022),
(3, 1, 2, 2022),
(7, 1, 5, 2022),
(8, 1, 3, 2022),
(9, 1, 2, 2022),
(10, 1, 2, 2022),
(9, 1, 5, 2021),
(6, 1, 4, 2021),
(1, 2, 4, 2021),
(1, 4, 5, 2020),
(6, 5, 3, 2022),
(1, 5, 5, 2022),
(2, 5, 5, 2022),
(3, 5, 3, 2022),
(6, 5, 2, 2022),
(1, 5, 3, 2022),
(2, 5, 4, 2022),
(3, 5, 5, 2022),
(6, 6, 5, 2022),
(1, 6, 5, 2022),
(2, 6, 3, 2022),
(3, 6, 5, 2022),
(7, 6, 4, 2022),
(8, 6, 5, 2022),
(9, 6, 5, 2022),
(10, 6, 5, 2022),
(6, 6, 4, 2022),
(1, 6, 5, 2022),
(2, 6, 4, 2022),
(3, 6, 5, 2022),
(7, 6, 5, 2022),
(8, 6, 5, 2022),
(9, 6, 5, 2022),
(10, 6, 5, 2022),
(6, 4, 4, 2021),
(2, 6, 4, 2021),
(1, 6, 5, 2021);

-- --------------------------------------------------------

--
-- Table structure for table `Code`
--

CREATE TABLE `Code` (
  `id` int(11) NOT NULL,
  `course` int(11) NOT NULL,
  `used_by_student` int(11) DEFAULT NULL,
  `code` varchar(255) NOT NULL,
  `year_of_validity` year(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `Code`
--

INSERT INTO `Code` (`id`, `course`, `used_by_student`, `code`, `year_of_validity`) VALUES
(1, 1, NULL, 'survey123', 2022),
(2, 1, 1, 'used-and-washed-up', 2021),
(3, 1, 2, 'used-up', 2022),
(4, 1, NULL, 'philosophical-questions', 2022),
(5, 1, NULL, '2022-2623-0', 2022),
(6, 1, NULL, '2022-2623-1', 2022),
(7, 1, NULL, '2022-2623-2', 2022),
(8, 1, NULL, '2022-2623-3', 2022),
(9, 1, NULL, '2022-2623-4', 2022),
(10, 1, NULL, '2022-2623-5', 2022),
(11, 1, NULL, '2022-2623-6', 2022),
(12, 1, NULL, '2022-2623-7', 2022),
(13, 1, NULL, '2022-2623-8', 2022),
(14, 1, NULL, '2022-2623-9', 2022),
(15, 5, 1, '2022-6613-0', 2022),
(16, 5, 2, '2022-6613-1', 2022),
(17, 5, NULL, '2022-6613-2', 2022),
(18, 5, NULL, '2022-6613-3', 2022),
(19, 6, 1, '2022-2843-0', 2022),
(20, 6, 2, '2022-2843-1', 2022),
(21, 6, NULL, '2022-2843-2', 2022),
(22, 6, NULL, '2022-2843-3', 2022);

-- --------------------------------------------------------

--
-- Table structure for table `Course`
--

CREATE TABLE `Course` (
  `id` int(11) NOT NULL,
  `teacher` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `Course`
--

INSERT INTO `Course` (`id`, `teacher`, `name`) VALUES
(1, 3, 'Φιλοσοφία'),
(2, 3, 'Κοινωνιολογία'),
(3, 3, 'Αρχαία Κινέζικα'),
(4, 5, 'Κυβερνοασφάλεια'),
(5, 7, 'Βυζαντινή Αποχετευτική'),
(6, 8, 'Τακτικές Guerrilla');

-- --------------------------------------------------------

--
-- Table structure for table `Question`
--

CREATE TABLE `Question` (
  `id` int(11) NOT NULL,
  `order_position` int(11) NOT NULL,
  `prompt` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `Question`
--

INSERT INTO `Question` (`id`, `order_position`, `prompt`) VALUES
(1, 101, 'Ο καθηγητής σας βοηθήσε να καταλάβετε το αντικείμενο του μαθήματος μέσω των διαλέξεων του'),
(2, 102, 'Ο καθηγητής παρείχε βοηθητικές σημειώσεις'),
(3, 103, 'Ο καθηγητής ήταν συνεπής στις υποχρεώσεις του'),
(6, 100, 'Ο καθηγητής σας διέγειρε το ενδιαφέρον για το μάθημα'),
(7, 200, '<i> (Αν ανατέθηκε κάποια εργασία) </i> Οι εργασίες είχαν ενδιαφέρον'),
(8, 201, '<i> (Αν ανατέθηκε κάποια εργασία) </i> Οι εργασίες ήταν καλή εξάσκηση'),
(9, 202, '<i> (Αν ανατέθηκε κάποια εργασία) </i> Οι εργασίες είχαν ξεκάθαρους στόχους και προδιαγραφές'),
(10, 203, '<i> (Αν ανατέθηκε κάποια εργασία και βαθμολογήθηκε) </i> Η βαθμολόγηση των εργασιών ήταν δίκαιη και διαφανής');

-- --------------------------------------------------------

--
-- Table structure for table `User`
--

CREATE TABLE `User` (
  `id` int(11) NOT NULL,
  `username` varchar(32) NOT NULL,
  `password` varchar(320) NOT NULL,
  `name` varchar(64) NOT NULL,
  `surname` varchar(64) NOT NULL,
  `type` enum('admin','student','teacher') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `User`
--

INSERT INTO `User` (`id`, `username`, `password`, `name`, `surname`, `type`) VALUES
(1, 'firstystudenty', 'password123', 'Πρωτέας', 'Πρωτευόμενος', 'student'),
(2, 'pparker', 'iamspiderman', 'Πέτρος', 'Πάρκος', 'student'),
(3, 'euhara', 'haranaxei', 'Ευλάμπιος', 'Χαράλαμπος', 'teacher'),
(4, 'the_plague', 'neverfeariishere', 'Eugene', 'Belford', 'admin'),
(5, 'cheeki_breeki', 'babooshka', 'Προμύσλαβ', 'Βαταρόβιτς', 'teacher'),
(6, 'macjanitor', 'secretingredient', 'Μάκης', 'Καθαριάκης', 'teacher'),
(7, 'katakouzina', 'seismos', 'Κωσταντίνος', 'Κατακουζινός', 'teacher'),
(8, 'mparmpastrumf', 'la la lala la la', 'Μπάρμπας', 'Στρουμφ', 'teacher');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Answer`
--
ALTER TABLE `Answer`
  ADD KEY `year` (`year`),
  ADD KEY `question` (`question`),
  ADD KEY `course` (`course`);

--
-- Indexes for table `Code`
--
ALTER TABLE `Code`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `code` (`code`),
  ADD UNIQUE KEY `voting_limit` (`course`,`used_by_student`,`year_of_validity`),
  ADD KEY `course` (`course`),
  ADD KEY `used_by_student` (`used_by_student`);

--
-- Indexes for table `Course`
--
ALTER TABLE `Course`
  ADD PRIMARY KEY (`id`),
  ADD KEY `teacher` (`teacher`);

--
-- Indexes for table `Question`
--
ALTER TABLE `Question`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `order_position` (`order_position`);

--
-- Indexes for table `User`
--
ALTER TABLE `User`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD KEY `type` (`type`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Code`
--
ALTER TABLE `Code`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `Course`
--
ALTER TABLE `Course`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `Question`
--
ALTER TABLE `Question`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `User`
--
ALTER TABLE `User`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `Answer`
--
ALTER TABLE `Answer`
  ADD CONSTRAINT `Answer_ibfk_1` FOREIGN KEY (`question`) REFERENCES `Question` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Answer_ibfk_2` FOREIGN KEY (`course`) REFERENCES `Course` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `Code`
--
ALTER TABLE `Code`
  ADD CONSTRAINT `Code_ibfk_1` FOREIGN KEY (`course`) REFERENCES `Course` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Code_ibfk_2` FOREIGN KEY (`used_by_student`) REFERENCES `User` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `Course`
--
ALTER TABLE `Course`
  ADD CONSTRAINT `Course_ibfk_1` FOREIGN KEY (`teacher`) REFERENCES `User` (`id`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

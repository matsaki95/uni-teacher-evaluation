```mermaid
erDiagram

User {
	int id pk
	string username
	string password
	string name
	string surname
	enum type "admin,student,teacher"
}
User ||--o| Admin: "is a"
User ||--o| Teacher: "is a"
User ||--o| Student: "is a"

Admin

Teacher
Teacher ||--o{ Course: teaches

Student

Code {
	int id pk
	int course fk
	int used_by_student fk
	string code
	year year_of_validity
}
Student ||--o{ Code: uses
Course ||--o{ Code: has

Course {
	int id pk
	int teacher fk
	string name
}

Answer {
	int question fk
	int course fk
	int rating
	year year
}
Answer o{--|| Question: answers
Answer o{--|| Course: "in context of"

Question {
	int id pk
	int order_position
	string text
}
```

Answer rating is in range of 1..=5